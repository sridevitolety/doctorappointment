function dateConvert(){
    
    //Convert Date to mm-dd-yyyy format
    var today = new Date();           
    var month = today .getMonth() + 1;  
    if(month<10)          month = "0"+month;
    var day = today .getDate();
    if(day<10)          day = "0"+day;
    var year = today .getFullYear();            
    var todayDate = year + "-" + month + "-" + day;
    return todayDate;
    //End of conversion  
}