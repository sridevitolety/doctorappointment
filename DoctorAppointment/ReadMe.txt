Doctor Appointment Application
-------------------------------

This is a platform for doctors and patients to have  online consultation.
Allows registrations from all stake holders and perform respective activites by logging in.

Modules
-------
Register User
Login
Doctor
Patient

Good Practices
-------------

1. Maintined granular folder structure
2. Clean coding with consistency.
3. Comments where ever required.
4. Right naming conventions.
5. Validating all the forms.